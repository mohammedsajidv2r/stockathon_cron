﻿using Newtonsoft.Json;
using Stockathon_Cron.ClassData;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using US_Stock_Cron.ClassData;

namespace US_Stock_Cron
{
    public partial class Stockathon_Cron : Form
    {
        public string ExeCaption_ = "";
        public Stockathon_Cron(string ExeCaption)
        {
            ExeCaption_ = ExeCaption;
            InitializeComponent();
        }

        private void Stockathon_Load(object sender, EventArgs e)
        {
            BaseConnection connection = new BaseConnection();
            try
            {
                FinnSettings settings = new FinnSettings();
                settings.ApiKey = ConfigurationManager.AppSettings["ApiKey"];
                settings.BaseURL = ConfigurationManager.AppSettings["BaseURL"];
                settings.Version = "v1";

                if (ExeCaption_.ToLower() == "adminstockcron")
                {
                    List<Symbol> symbols = connection.getSymbol("selectsymbol", "Jobs_USP_Admin_Stock");
                    foreach (var symbol in symbols)
                    {
                        //companyprofile
                        ExeCaption_ = "insertcompanyprofile";
                        try
                        {
                            CompanyProfile cp = ApiClass.CompanyProfile(settings, symbol.symbol);
                            cp.description = "" + cp.description + "";
                            cp.symbol = symbol.symbol;
                            cp.stock_symbol_id = symbol.stock_symbol_id;
                            var XMLData = XmlConversion.SerializeToXElement(cp);
                            connection.InsetData(ExeCaption_, "Jobs_USP_Admin_Stock", XMLData.ToString(), symbol.id.ToString());
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Admin_Stock", symbol.symbol + "," + symbol.id.ToString(), 0, ex.Message.ToString());
                        }

                        //executive
                        ExeCaption_ = "insertexecutive";
                        try
                        {
                            Tbl_executive execList = ApiClass.Executive(settings, symbol.symbol);
                            execList.executive = execList.executive.Select(s => { s.symbol = symbol.symbol; s.stock_symbol_id = symbol.stock_symbol_id; return s; }).ToList();
                            var XMLData = XmlConversion.SerializeToXElement(execList);
                            connection.InsetData(ExeCaption_, "Jobs_USP_Admin_Stock", XMLData.ToString(), symbol.id.ToString());
                            //Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Admin_Stock", symbol.symbol + "," + symbol.id.ToString(), 0, ex.Message.ToString());
                        }

                        //peers
                        ExeCaption_ = "insertpeers";
                        try
                        {
                            List<String> peerList = ApiClass.Peers(settings, symbol.symbol);
                            List<peerList> peerListClass = new List<peerList>();
                            foreach (var peerLists in peerList)
                            {
                                peerList pp = new peerList();
                                pp.stock_symbol_id = symbol.stock_symbol_id;
                                pp.symbol = symbol.symbol;
                                pp.peer = peerLists;
                                peerListClass.Add(pp);
                            }

                            var XMLData = XmlConversion.SerializeToXElement(peerListClass);
                            connection.InsetData(ExeCaption_, "Jobs_USP_Admin_Stock", XMLData.ToString(), symbol.id.ToString());
                            //Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Admin_Stock", symbol.symbol + "," + symbol.id.ToString(), 0, ex.Message.ToString());
                        }

                        //ownership
                        ExeCaption_ = "insertownership";
                        try
                        {
                            OwnershipDetails execList = ApiClass.Ownership(settings, symbol.symbol, "20", "");
                            execList.Ownership = execList.Ownership.Select(s => { s.symbol = symbol.symbol; s.stock_symbol_id = symbol.stock_symbol_id; return s; }).ToList();
                            var XMLData = XmlConversion.SerializeToXElement(execList);
                            connection.InsetData(ExeCaption_, "Jobs_USP_Admin_Stock", XMLData.ToString(), symbol.id.ToString());
                            //Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Admin_Stock", symbol.symbol + "," + symbol.id.ToString(), 0, ex.Message.ToString());
                        }

                        //fundownership
                        ExeCaption_ = "insertfundownership";
                        try
                        {
                            OwnershipDetails execList = ApiClass.Ownership(settings, symbol.symbol, "", "fund");
                            execList.Ownership = execList.Ownership.Select(s => { s.symbol = symbol.symbol; s.stock_symbol_id = symbol.stock_symbol_id; return s; }).ToList();
                            var XMLData = XmlConversion.SerializeToXElement(execList);
                            connection.InsetData(ExeCaption_, "Jobs_USP_Admin_Stock", XMLData.ToString(), symbol.id.ToString());
                            Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Admin_Stock", symbol.symbol + "," + symbol.id.ToString(), 0, ex.Message.ToString());
                        }

                        //financialbs annually
                        try
                        {
                            FinancialsData finListBs = ApiClass.Financials(settings, symbol.symbol, "bs", "annual");
                            finListBs.FinancialsBs = finListBs.FinancialsBs.Select(s => { s.symbol = symbol.symbol; s.stock_symbol_id = symbol.stock_symbol_id; s.freq = "annual"; return s; }).ToList();
                            var XMLDataBs = XmlConversion.SerializeToXElement(finListBs);
                            ExeCaption_ = "insertfinancialbs";
                            connection.InsetData(ExeCaption_, "Jobs_USP_Admin_Stock", XMLDataBs.ToString(), symbol.id.ToString());
                            //Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Admin_Stock", symbol.symbol + "," + symbol.id.ToString(), 0, ex.Message.ToString());
                        }

                        //financialic annually
                        try
                        {
                            FinancialsData finListIc = ApiClass.Financials(settings, symbol.symbol, "ic", "annual");
                            finListIc.FinancialslistIc = finListIc.FinancialslistIc.Select(s => { s.symbol = symbol.symbol; s.stock_symbol_id = symbol.stock_symbol_id; s.freq = "annual"; return s; }).ToList();
                            var XMLDataIc = XmlConversion.SerializeToXElement(finListIc);
                            ExeCaption_ = "insertfinancialic";
                            connection.InsetData(ExeCaption_, "Jobs_USP_Admin_Stock", XMLDataIc.ToString(), symbol.id.ToString());
                            //Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Admin_Stock", symbol.symbol + "," + symbol.id.ToString(), 0, ex.Message.ToString());
                        }

                        //financialcf annually
                        try
                        {
                            FinancialsData finListCf = ApiClass.Financials(settings, symbol.symbol, "cf", "annual");
                            finListCf.FinancialslistCf = finListCf.FinancialslistCf.Select(s => { s.symbol = symbol.symbol; s.stock_symbol_id = symbol.stock_symbol_id; s.freq = "annual"; return s; }).ToList();
                            var XMLDataCf = XmlConversion.SerializeToXElement(finListCf);
                            ExeCaption_ = "insertfinancialcf";
                            connection.InsetData(ExeCaption_, "Jobs_USP_Admin_Stock", XMLDataCf.ToString(), symbol.id.ToString());
                            //Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Admin_Stock", symbol.symbol + "," + symbol.id.ToString(), 0, ex.Message.ToString());
                        }

                        //financialbs quarterly
                        try
                        {
                            FinancialsData finListBs = ApiClass.Financials(settings, symbol.symbol, "bs", "quarterly");
                            finListBs.FinancialsBs = finListBs.FinancialsBs.Select(s => { s.symbol = symbol.symbol; s.stock_symbol_id = symbol.stock_symbol_id; s.freq = "quarterly"; return s; }).ToList();
                            var XMLDataBs = XmlConversion.SerializeToXElement(finListBs);
                            ExeCaption_ = "insertfinancialbs";
                            connection.InsetData(ExeCaption_, "Jobs_USP_Admin_Stock", XMLDataBs.ToString(), symbol.id.ToString());
                            Thread.Sleep(10);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Admin_Stock", symbol.symbol + "," + symbol.id.ToString(), 0, ex.Message.ToString());
                        }

                        //financialic quarterly
                        try
                        {
                            FinancialsData finListIc = ApiClass.Financials(settings, symbol.symbol, "ic", "quarterly");
                            finListIc.FinancialslistIc = finListIc.FinancialslistIc.Select(s => { s.symbol = symbol.symbol; s.stock_symbol_id = symbol.stock_symbol_id; s.freq = "quarterly"; return s; }).ToList();
                            var XMLDataIc = XmlConversion.SerializeToXElement(finListIc);
                            ExeCaption_ = "insertfinancialic";
                            connection.InsetData(ExeCaption_, "Jobs_USP_Admin_Stock", XMLDataIc.ToString(), symbol.id.ToString());
                            Thread.Sleep(10);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Admin_Stock", symbol.symbol + "," + symbol.id.ToString(), 0, ex.Message.ToString());
                        }

                        //financialcf quarterly
                        try
                        {
                            FinancialsData finListCf = ApiClass.Financials(settings, symbol.symbol, "cf", "quarterly");
                            finListCf.FinancialslistCf = finListCf.FinancialslistCf.Select(s => { s.symbol = symbol.symbol; s.stock_symbol_id = symbol.stock_symbol_id; s.freq = "quarterly"; return s; }).ToList();
                            var XMLDataCf = XmlConversion.SerializeToXElement(finListCf);
                            ExeCaption_ = "insertfinancialcf";
                            connection.InsetData(ExeCaption_, "Jobs_USP_Admin_Stock", XMLDataCf.ToString(), symbol.id.ToString());
                            Thread.Sleep(10);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Admin_Stock", symbol.symbol + "," + symbol.id.ToString(), 0, ex.Message.ToString());
                        }

                        //quote
                        ExeCaption_ = "insertquote";
                        try
                        {
                            Quote quote = ApiClass.Quote(settings, symbol.symbol);
                            quote.symbol = symbol.symbol;
                            quote.stock_symbol_id = symbol.stock_symbol_id;
                            quote.unixtodate = ApiClass.UnixTimeStampToDateTime(quote.t);

                            var XMLData = XmlConversion.SerializeToXElement(quote);
                            connection.InsetData(ExeCaption_, "Jobs_USP_Admin_Stock", XMLData.ToString(), symbol.id.ToString());
                            Thread.Sleep(650);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Admin_Stock", symbol.symbol + "," + symbol.id.ToString(), 0, ex.Message.ToString());
                        }

                        //Price Target
                        ExeCaption_ = "insertpricetarget";
                        try
                        {
                            // Target quote = ApiClass.Target(settings, symbol.symbol);
                            Target quote = ApiClass.Target(settings, symbol.symbol);
                            quote.stock_symbol_id = symbol.stock_symbol_id;
                            quote.symbol = symbol.symbol;
                            var XMLData = XmlConversion.SerializeToXElement(quote);
                            connection.InsetData(ExeCaption_, "Jobs_USP_Admin_Stock", XMLData.ToString(), symbol.id.ToString());
                            Thread.Sleep(650);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Admin_Stock", symbol.symbol + "," + symbol.id.ToString(), 0, ex.Message.ToString());
                        }

                        //Candle monthly
                        ExeCaption_ = "insertcandlemonthly";
                        try
                        {
                            var today = DateTime.Today;
                            var month = new DateTime(today.Year, today.Month, 1);
                            var first = month.AddMonths(-1);
                            var last = month.AddDays(-1);
                            var from = new DateTimeOffset(first).ToUnixTimeSeconds();
                            var to = new DateTimeOffset(last).ToUnixTimeSeconds();
                            //for 10 year range
                            Candle quote = ApiClass.Candle(settings, symbol.symbol, "M", "1291113907", to.ToString());
                            quote.t = quote.t.Select(x => { x = ApiClass.UnixTimeStampToDateTime(double.Parse(x)).ToString(); return x; }).ToList();
                            List<candleList> candleListClass = Candle(quote, symbol);
                            var XMLData = XmlConversion.SerializeToXElement(candleListClass);
                            connection.InsetData(ExeCaption_, "Jobs_USP_Admin_Stock", XMLData.ToString(), symbol.id.ToString());
                            Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Admin_Stock", symbol.symbol + "," + symbol.id.ToString(), 0, ex.Message.ToString());
                        }

                        //Candle Weekly
                        ExeCaption_ = "insertcandleweekly";
                        try
                        {
                            DayOfWeek weekStart = DayOfWeek.Sunday; // or Sunday, or whenever
                            DateTime startingDate = DateTime.Today;

                            while (startingDate.DayOfWeek != weekStart)
                                startingDate = startingDate.AddDays(-1);

                            DateTime previousWeekStart = startingDate.AddDays(-7).AddHours(12);
                            DateTime previousWeekEnd = startingDate.AddHours(12);
                            var from = new DateTimeOffset(previousWeekStart).ToUnixTimeSeconds();
                            var to = new DateTimeOffset(previousWeekEnd).ToUnixTimeSeconds();

                            //for 10 year range
                            Candle quote = ApiClass.Candle(settings, symbol.symbol, "W", "1291113907", to.ToString());

                            quote.t = quote.t.Select(x => { x = ApiClass.UnixTimeStampToDateTime(double.Parse(x)).ToString(); return x; }).ToList();
                            List<candleList> candleListClass = Candle(quote, symbol);
                            var XMLData = XmlConversion.SerializeToXElement(candleListClass);
                            connection.InsetData(ExeCaption_, "Jobs_USP_Admin_Stock", XMLData.ToString(), symbol.id.ToString());
                            Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Admin_Stock", symbol.symbol + "," + symbol.id.ToString(), 0, ex.Message.ToString());
                        }

                        //Candle daily
                        ExeCaption_ = "insertcandledaily";
                        try
                        {
                            DateTime previousWeekStart = DateTime.Now.Date.AddDays(-2).AddHours(12);
                            DateTime previousWeekEnd = DateTime.Now.Date.AddDays(-1).AddHours(12);
                            var from = new DateTimeOffset(previousWeekStart).ToUnixTimeSeconds();
                            var to = new DateTimeOffset(previousWeekEnd).ToUnixTimeSeconds();
                            //for 10 year range
                            Candle quote = ApiClass.Candle(settings, symbol.symbol, "D", "1291870716", to.ToString());

                            quote.t = quote.t.Select(x => { x = ApiClass.UnixTimeStampToDateTime(double.Parse(x)).ToString(); return x; }).ToList();
                            List<candleList> candleListClass = Candle(quote, symbol);
                            var XMLData = XmlConversion.SerializeToXElement(candleListClass);
                            connection.InsetData(ExeCaption_, "Jobs_USP_Admin_Stock", XMLData.ToString(), symbol.id.ToString());
                            Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Admin_Stock", symbol.symbol + "," + symbol.id.ToString(), 0, ex.Message.ToString());
                        }

                        //basic financial
                        try
                        {
                            BasicFinancials bf = ApiClass.BasicFinancials(settings, symbol.symbol, "all");
                            bf.metric.symbol = symbol.symbol;
                            bf.metric.stock_symbol_id = symbol.stock_symbol_id;
                            var XMLData = XmlConversion.SerializeToXElement(bf);
                            connection.InsetData(ExeCaption_, "Jobs_USP_Admin_Stock", XMLData.ToString(), symbol.id.ToString());
                            Thread.Sleep(500);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Admin_Stock", symbol.symbol + "," + symbol.id.ToString(), 0, ex.Message.ToString());
                        }

                        //dividend
                        ExeCaption_ = "insertdividend";
                        string fromD = "2010-12-16";
                        DateTime toD = DateTime.Now.Date;
                        try
                        {
                            List<Dividend> divList = ApiClass.Dividend(settings, symbol.symbol, fromD, toD.ToString("yyyy-MM-dd"));
                            divList = divList.Select(s => { s.stock_symbol_id = symbol.stock_symbol_id; return s; }).ToList();
                            var XMLData = XmlConversion.SerializeToXElement(divList);
                            connection.InsetData(ExeCaption_, "Jobs_USP_Admin_Stock", XMLData.ToString(), symbol.id.ToString());
                            Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Admin_Stock", symbol.symbol + "," + symbol.id.ToString(), 0, ex.Message.ToString());
                        }

                        ExeCaption_ = "insertfilings";
                        try
                        {
                            List<Filing> FilingList = ApiClass.Filings(settings, symbol.symbol);
                            FilingList = FilingList.Select(s => { s.stock_symbol_id = symbol.stock_symbol_id; return s; }).ToList();
                            var XMLData = XmlConversion.SerializeToXElement(FilingList);
                            connection.InsetData(ExeCaption_, "Jobs_USP_Admin_Stock", XMLData.ToString(), symbol.id.ToString());
                            Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Admin_Stock", symbol.symbol + "," + symbol.id.ToString(), 0, ex.Message.ToString());
                        }

                        ExeCaption_ = "insertbasicfinancials";
                        try
                        {
                            BasicFinancials bf = ApiClass.BasicFinancials(settings, symbol.symbol, "all");
                            bf.metric.symbol = symbol.symbol;
                            bf.metric.stock_symbol_id = symbol.stock_symbol_id;
                            var XMLData = XmlConversion.SerializeToXElement(bf);
                            connection.InsetData(ExeCaption_, "Jobs_USP_Admin_Stock", XMLData.ToString(), symbol.id.ToString());
                            Thread.Sleep(500);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Admin_Stock", symbol.symbol + "," + symbol.id.ToString(), 0, ex.Message.ToString());
                        }

                        //StocksAdd
                        ExeCaption_ = "stockupdateend";
                        try
                        {
                            connection.InsetData(ExeCaption_, "Jobs_USP_Admin_Stock", "", symbol.id.ToString());
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Admin_Stock", symbol.symbol + "," + symbol.id.ToString(), 0, ex.Message.ToString());
                        }

                    }
                }

                else if (ExeCaption_.ToLower() == "insertownership")
                {
                    List<Symbol> symbols = connection.getSymbol("selectsymbolownership", "USP_Stock_Symbol");
                    foreach (var symbol in symbols)
                    {
                        try
                        {
                            OwnershipDetails execList = ApiClass.Ownership(settings, symbol.symbol, "20", "");
                            execList.Ownership = execList.Ownership.Select(s => { s.symbol = symbol.symbol; s.stock_symbol_id = symbol.stock_symbol_id; return s; }).ToList();
                            var XMLData = XmlConversion.SerializeToXElement(execList);
                            connection.InsetData(ExeCaption_, "USP_Stock_Symbol", XMLData.ToString(), "");
                            Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "USP_Stock_Symbol", symbol.symbol, 0, ex.Message.ToString());
                        }
                    }
                }

                else if (ExeCaption_.ToLower() == "insertquote")
                {
                    List<Symbol> symbols = connection.getSymbol("selectsymbolquoted", "Jobs_USP_Stock_Price");
                    foreach (var symbol in symbols)
                    {
                        try
                        {
                            Quote quote = ApiClass.Quote(settings, symbol.symbol);
                            quote.symbol = symbol.symbol;
                            quote.stock_symbol_id = symbol.stock_symbol_id;
                            quote.unixtodate = ApiClass.UnixTimeStampToDateTime(quote.t);

                            var XMLData = XmlConversion.SerializeToXElement(quote);
                            connection.InsetData(ExeCaption_, "Jobs_USP_Stock_Price", XMLData.ToString(), "");
                            Thread.Sleep(1000);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Stock_Price", symbol.symbol, 0, ex.Message.ToString());
                        }
                    }
                }

                else if (ExeCaption_.ToLower() == "insertcandleweekly")
                {
                    List<Symbol> symbols = connection.getSymbol("selectsymbolcandleweekly", "Jobs_USP_Stock_Price");
                    foreach (var symbol in symbols)
                    {
                        try
                        {
                            DayOfWeek weekStart = DayOfWeek.Sunday; // or Sunday, or whenever
                            DateTime startingDate = DateTime.Today;

                            while (startingDate.DayOfWeek != weekStart)
                                startingDate = startingDate.AddDays(-1);

                            DateTime previousWeekStart = startingDate.AddDays(-7).AddHours(12);
                            DateTime previousWeekEnd = startingDate.AddHours(12);
                            var from = new DateTimeOffset(previousWeekStart).ToUnixTimeSeconds();
                            var to = new DateTimeOffset(previousWeekEnd).ToUnixTimeSeconds();
                            Candle quote = ApiClass.Candle(settings, symbol.symbol, "W", from.ToString(), to.ToString());

                            //Candle quote = ApiClass.Candle(settings, symbol.symbol, "W", "1616065733", to.ToString());

                            //for 10 year range
                            //Candle quote = ApiClass.Candle(settings, symbol.symbol, "W", "1291113907", "1606733107");

                            quote.t = quote.t.Select(x => { x = ApiClass.UnixTimeStampToDateTime(double.Parse(x)).ToString(); return x; }).ToList();
                            List<candleList> candleListClass = Candle(quote, symbol);
                            var XMLData = XmlConversion.SerializeToXElement(candleListClass);
                            connection.InsetData(ExeCaption_, "Jobs_USP_Stock_Price", XMLData.ToString(), "");
                           Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            if (!ex.Message.ToString().Contains("Value cannot be null.\r\nParameter name: source"))
                            {
                                connection.InsertLogs(ExeCaption_, "Jobs_USP_Stock_Price", symbol.symbol, 0, ex.Message.ToString());
                            }
                        }
                    }
                }

                else if (ExeCaption_.ToLower() == "insertcandledaily")
                {
                    List<Symbol> symbols = connection.getSymbol("selectsymbolcandledaily", "Jobs_USP_Stock_Price");
                    foreach (var symbol in symbols)
                    {
                        try
                        {
                            DateTime previousWeekStart = DateTime.Now.Date.AddDays(-2).AddHours(12);
                            DateTime previousWeekEnd = DateTime.Now.Date.AddDays(-1).AddHours(10);
                            //DateTime previousWeekEnd = DateTime.Now.Date.AddHours(12);

                            if (previousWeekStart.DayOfWeek.ToString() == "Saturday" && previousWeekEnd.DayOfWeek.ToString() == "Sunday")
                            {
                                previousWeekStart = previousWeekStart.AddDays(-1);
                                previousWeekEnd = previousWeekEnd.AddDays(-1);
                            }
                            var from = new DateTimeOffset(previousWeekStart).ToUnixTimeSeconds();
                            var to = new DateTimeOffset(previousWeekEnd).ToUnixTimeSeconds();
                            //LogUtil.MakeCustomLog(string.Format("{0} insertcandledaily ", ""), from.ToString() + '-' + to.ToString());

                            //Candle quote = ApiClass.Candle(settings, symbol.symbol, "D", from.ToString(), to.ToString());
                            Candle quote = ApiClass.Candle(settings, symbol.symbol, "D", "1622246400", to.ToString());
                            //Candle quote = ApiClass.Candle(settings, symbol.symbol, "D", "1619614800", to.ToString());
                            //for 10 year range
                            //Candle quote = ApiClass.Candle(settings, symbol.symbol, "D", "1291870716", "1607342400");

                            quote.t = quote.t.Select(x => { x = ApiClass.UnixTimeStampToDateTime(double.Parse(x)).ToString(); return x; }).ToList();
                            List<candleList> candleListClass = Candle(quote, symbol);
                            var XMLData = XmlConversion.SerializeToXElement(candleListClass);
                            connection.InsetData(ExeCaption_, "Jobs_USP_Stock_Price", XMLData.ToString(), "");
                            Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            if (!ex.Message.ToString().Contains("Value cannot be null.\r\nParameter name: source"))
                            {
                                connection.InsertLogs(ExeCaption_, "Jobs_USP_Stock_Price", symbol.symbol, 0, ex.Message.ToString());
                            }
                        }
                    }

                }

                else if (ExeCaption_.ToLower() == "marketnews")
                {
                    try
                    {
                        List<News> bf = ApiClass.News(settings, "general");
                        bf = bf.Select(x => { x.datetime = ApiClass.UnixTimeStampToDateTime(double.Parse(x.datetime)).ToString(); x.headline = RemoveDiacritics(x.headline); x.summary = RemoveDiacritics(x.summary); return x; }).ToList();
                        bf = bf.OrderByDescending(x => Convert.ToDateTime(x.datetime)).Take(10).ToList();
                        var XMLData = XmlConversion.SerializeToXElement(bf);
                        connection.InsetData(ExeCaption_, "Jobs_USP_Stock_Price", XMLData.ToString(), "");
                    }
                    catch (Exception ex)
                    {
                        connection.InsertLogs(ExeCaption_, "Jobs_USP_Stock_Price", "marketnews", 0, ex.Message.ToString());
                    }
                }

                else if (ExeCaption_.ToLower() == "insertdividend")
                {
                    List<Symbol> symbols = connection.getSymbol("selectsymbodiv", "USP_Stock_Symbol");
                    string from = "2010-12-16";
                    DateTime to = DateTime.Now.Date;
                    foreach (var symbol in symbols)
                    {
                        try
                        {
                            List<Dividend> divList = ApiClass.Dividend(settings, symbol.symbol, from, to.ToString("yyyy-MM-dd"));
                            divList = divList.Select(s => { s.stock_symbol_id = symbol.stock_symbol_id; return s; }).ToList();
                            var XMLData = XmlConversion.SerializeToXElement(divList);
                            connection.InsetData(ExeCaption_, "USP_Stock_Symbol", XMLData.ToString(), "");
                            Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "USP_Stock_Symbol", symbol.symbol, 0, ex.Message.ToString());
                        }
                    }
                }

                else if (ExeCaption_.ToLower() == "insertcandlemonthly")
                {
                    List<Symbol> symbols = connection.getSymbol("selectsymbolcandlemonthly", "Jobs_USP_Stock_Price");
                    foreach (var symbol in symbols)
                    {
                        try
                        {
                            var today = DateTime.Today;
                            var month = new DateTime(today.Year, today.Month, 1);
                            var first = month.AddMonths(-1);
                            var last = month.AddDays(-1);
                            var from = new DateTimeOffset(first).ToUnixTimeSeconds();
                            var to = new DateTimeOffset(last).ToUnixTimeSeconds();
                            //Candle quote = ApiClass.Candle(settings, symbol.symbol, "M", from.ToString(), to.ToString());
                            Candle quote = ApiClass.Candle(settings, symbol.symbol, "M", "1610451365", "1615437000");

                            //for 10 year range
                            //Candle quote = ApiClass.Candle(settings, symbol.symbol, "M", "1291113907", "1606733107");

                            quote.t = quote.t.Select(x => { x = ApiClass.UnixTimeStampToDateTime(double.Parse(x)).ToString(); return x; }).ToList();
                            List<candleList> candleListClass = Candle(quote, symbol);
                            var XMLData = XmlConversion.SerializeToXElement(candleListClass);
                            connection.InsetData(ExeCaption_, "Jobs_USP_Stock_Price", XMLData.ToString(), "");
                            Thread.Sleep(1000);
                        }
                        catch (Exception ex)
                        {
                            if (!ex.Message.ToString().Contains("Value cannot be null.\r\nParameter name: source"))
                            {
                                connection.InsertLogs(ExeCaption_, "Jobs_USP_Stock_Price", symbol.symbol, 0, ex.Message.ToString());
                            }
                        }
                    }
                }

                #region
                else if (ExeCaption_.ToLower() == "insertstocksymbol")
                {
                    List<Symbol> symbols = ApiClass.StockSymbols(settings, "US");
                    var XMLData = XmlConversion.SerializeToXElement(symbols);
                    connection.InsetData(ExeCaption_, "USP_Stock_Symbol", XMLData.ToString(), "");
                }

                else if (ExeCaption_.ToLower() == "insertcompanyprofile")
                {
                    List<Symbol> symbols = connection.getSymbol("selectsymbol", "USP_Stock_Symbol");
                    foreach (var symbol in symbols)
                    {
                        try
                        {
                            CompanyProfile cp = ApiClass.CompanyProfile(settings, symbol.symbol);
                            cp.description = "" + cp.description + "";
                            cp.symbol = symbol.symbol;
                            cp.stock_symbol_id = symbol.stock_symbol_id;
                            var XMLData = XmlConversion.SerializeToXElement(cp);
                            connection.InsetData(ExeCaption_, "USP_Stock_Symbol", XMLData.ToString(), "");
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "USP_Stock_Symbol", symbol.symbol, 0, ex.Message.ToString());
                        }
                    }
                }

                else if (ExeCaption_.ToLower() == "insertexecutive")
                {
                    List<Symbol> symbols = connection.getSymbol("selectsymbolExecutive", "USP_Stock_Symbol");
                    foreach (var symbol in symbols)
                    {
                        try
                        {
                            Tbl_executive execList = ApiClass.Executive(settings, symbol.symbol);
                            execList.executive = execList.executive.Select(s => { s.symbol = symbol.symbol; s.stock_symbol_id = symbol.stock_symbol_id; return s; }).ToList();
                            var XMLData = XmlConversion.SerializeToXElement(execList);
                            connection.InsetData(ExeCaption_, "USP_Stock_Symbol", XMLData.ToString(), "");
                            Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "USP_Stock_Symbol", symbol.symbol, 0, ex.Message.ToString());
                        }
                    }
                }

                else if (ExeCaption_.ToLower() == "insertpeers")
                {
                    List<Symbol> symbols = connection.getSymbol("selectsymbolpeers", "USP_Stock_Symbol");
                    foreach (var symbol in symbols)
                    {
                        try
                        {
                            List<String> peerList = ApiClass.Peers(settings, symbol.symbol);
                            List<peerList> peerListClass = new List<peerList>();
                            foreach (var peerLists in peerList)
                            {
                                peerList pp = new peerList();
                                pp.stock_symbol_id = symbol.stock_symbol_id;
                                pp.symbol = symbol.symbol;
                                pp.peer = peerLists;
                                peerListClass.Add(pp);
                            }

                            var XMLData = XmlConversion.SerializeToXElement(peerListClass);
                            connection.InsetData(ExeCaption_, "USP_Stock_Symbol", XMLData.ToString(), "");
                            Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "USP_Stock_Symbol", symbol.symbol, 0, ex.Message.ToString());
                        }
                    }
                }

                else if (ExeCaption_.ToLower() == "insertfundownership")
                {
                    List<Symbol> symbols = connection.getSymbol("selectsymbolfundownership", "USP_Stock_Symbol");
                    foreach (var symbol in symbols)
                    {
                        try
                        {
                            OwnershipDetails execList = ApiClass.Ownership(settings, symbol.symbol, "", "fund");
                            execList.Ownership = execList.Ownership.Select(s => { s.symbol = symbol.symbol; s.stock_symbol_id = symbol.stock_symbol_id; return s; }).ToList();
                            var XMLData = XmlConversion.SerializeToXElement(execList);
                            connection.InsetData(ExeCaption_, "USP_Stock_Symbol", XMLData.ToString(), "");
                            Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "USP_Stock_Symbol", symbol.symbol, 0, ex.Message.ToString());
                        }
                    }
                }

                else if (ExeCaption_.ToLower() == "insertfinancial")
                {
                    List<Symbol> symbols = connection.getSymbol("selectsymbolfinancial", "USP_Stock_Symbol");
                    foreach (var symbol in symbols)
                    {
                        try
                        {
                            FinancialsData finListBs = ApiClass.Financials(settings, symbol.symbol, "bs", "annual");
                            finListBs.FinancialsBs = finListBs.FinancialsBs.Select(s => { s.symbol = symbol.symbol; s.stock_symbol_id = symbol.stock_symbol_id; s.freq = "annual"; return s; }).ToList();
                            var XMLDataBs = XmlConversion.SerializeToXElement(finListBs);
                            ExeCaption_ = "insertfinancialbs";
                            connection.InsetData(ExeCaption_, "USP_Stock_Symbol", XMLDataBs.ToString(), "");
                            //Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "USP_Stock_Symbol", symbol.symbol, 0, ex.Message.ToString());
                        }
                        try
                        {
                            FinancialsData finListIc = ApiClass.Financials(settings, symbol.symbol, "ic", "annual");
                            finListIc.FinancialslistIc = finListIc.FinancialslistIc.Select(s => { s.symbol = symbol.symbol; s.stock_symbol_id = symbol.stock_symbol_id; s.freq = "annual"; return s; }).ToList();
                            var XMLDataIc = XmlConversion.SerializeToXElement(finListIc);
                            ExeCaption_ = "insertfinancialic";
                            connection.InsetData(ExeCaption_, "USP_Stock_Symbol", XMLDataIc.ToString(), "");
                            //Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "USP_Stock_Symbol", symbol.symbol, 0, ex.Message.ToString());
                        }
                        try
                        {
                            FinancialsData finListCf = ApiClass.Financials(settings, symbol.symbol, "cf", "annual");
                            finListCf.FinancialslistCf = finListCf.FinancialslistCf.Select(s => { s.symbol = symbol.symbol; s.stock_symbol_id = symbol.stock_symbol_id; s.freq = "annual"; return s; }).ToList();
                            var XMLDataCf = XmlConversion.SerializeToXElement(finListCf);
                            ExeCaption_ = "insertfinancialcf";
                            connection.InsetData(ExeCaption_, "USP_Stock_Symbol", XMLDataCf.ToString(), "");
                            //Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "USP_Stock_Symbol", symbol.symbol, 0, ex.Message.ToString());
                        }

                    }
                }

                else if (ExeCaption_.ToLower() == "insertfinancialquart")
                {
                    List<Symbol> symbols = connection.getSymbol("selectsymbolfinancialquart", "USP_Stock_Symbol");
                    foreach (var symbol in symbols)
                    {
                        try
                        {
                            FinancialsData finListBs = ApiClass.Financials(settings, symbol.symbol, "bs", "quarterly");
                            finListBs.FinancialsBs = finListBs.FinancialsBs.Select(s => { s.symbol = symbol.symbol; s.stock_symbol_id = symbol.stock_symbol_id; s.freq = "quarterly"; return s; }).ToList();
                            var XMLDataBs = XmlConversion.SerializeToXElement(finListBs);
                            ExeCaption_ = "insertfinancialbs";
                            connection.InsetData(ExeCaption_, "USP_Stock_Symbol", XMLDataBs.ToString(), "");
                            Thread.Sleep(10);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "USP_Stock_Symbol", symbol.symbol, 0, ex.Message.ToString());
                        }
                        try
                        {
                            FinancialsData finListIc = ApiClass.Financials(settings, symbol.symbol, "ic", "quarterly");
                            finListIc.FinancialslistIc = finListIc.FinancialslistIc.Select(s => { s.symbol = symbol.symbol; s.stock_symbol_id = symbol.stock_symbol_id; s.freq = "quarterly"; return s; }).ToList();
                            var XMLDataIc = XmlConversion.SerializeToXElement(finListIc);
                            ExeCaption_ = "insertfinancialic";
                            connection.InsetData(ExeCaption_, "USP_Stock_Symbol", XMLDataIc.ToString(), "");
                            Thread.Sleep(10);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "USP_Stock_Symbol", symbol.symbol, 0, ex.Message.ToString());
                        }
                        try
                        {
                            FinancialsData finListCf = ApiClass.Financials(settings, symbol.symbol, "cf", "quarterly");
                            finListCf.FinancialslistCf = finListCf.FinancialslistCf.Select(s => { s.symbol = symbol.symbol; s.stock_symbol_id = symbol.stock_symbol_id; s.freq = "quarterly"; return s; }).ToList();
                            var XMLDataCf = XmlConversion.SerializeToXElement(finListCf);
                            ExeCaption_ = "insertfinancialcf";
                            connection.InsetData(ExeCaption_, "USP_Stock_Symbol", XMLDataCf.ToString(), "");
                            Thread.Sleep(10);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "USP_Stock_Symbol", symbol.symbol, 0, ex.Message.ToString());
                        }
                    }
                }

                else if (ExeCaption_.ToLower() == "insertpricetarget")
                {
                    List<Symbol> symbols = connection.getSymbol("selectsymboltarget", "Jobs_USP_Stock_Price");
                    foreach (var symbol in symbols)
                    {
                        try
                        {
                            Target quote = ApiClass.Target(settings, symbol.symbol);
                            quote.stock_symbol_id = symbol.stock_symbol_id;
                            quote.symbol = symbol.symbol;
                            var XMLData = XmlConversion.SerializeToXElement(quote);
                            connection.InsetData(ExeCaption_, "Jobs_USP_Stock_Price", XMLData.ToString(), "");
                            Thread.Sleep(650);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Stock_Price", symbol.symbol, 0, ex.Message.ToString());
                        }
                    }
                }


                else if (ExeCaption_.ToLower() == "insertbasicfinancials")
                {
                    List<Symbol> symbols = connection.getSymbol("selectsymbolfinancialmetric", "USP_Stock_Symbol");
                    foreach (var symbol in symbols)
                    {
                        try
                        {
                            BasicFinancials bf = ApiClass.BasicFinancials(settings, symbol.symbol, "all");
                            bf.metric.symbol = symbol.symbol;
                            bf.metric.stock_symbol_id = symbol.stock_symbol_id;
                            var XMLData = XmlConversion.SerializeToXElement(bf);
                            connection.InsetData(ExeCaption_, "USP_Stock_Symbol", XMLData.ToString(), "");
                            Thread.Sleep(500);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Stock_Price", symbol.symbol, 0, ex.Message.ToString());
                        }
                    }
                }

                else if (ExeCaption_.ToLower() == "insertfilings")
                {
                    List<Symbol> symbols = connection.getSymbol("selectsymbolfiling", "Jobs_USP_Stock_Price");
                    foreach (var symbol in symbols)
                    {
                        try
                        {
                            List<Filing> FilingList = ApiClass.Filings(settings, symbol.symbol);
                            FilingList = FilingList.Select(s => { s.stock_symbol_id = symbol.stock_symbol_id; return s; }).ToList();
                            var XMLData = XmlConversion.SerializeToXElement(FilingList);
                            connection.InsetData(ExeCaption_, "Jobs_USP_Stock_Price", XMLData.ToString(), "");
                            Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            connection.InsertLogs(ExeCaption_, "Jobs_USP_Stock_Price", symbol.symbol, 0, ex.Message.ToString());
                        }
                    }
                }

                else if (ExeCaption_.ToLower() == "piotroskidata")
                {
                    connection.InsetDatapiotroskidata(ExeCaption_, "Jobs_USP_Stock_Price", "", "");
                }

                else if (ExeCaption_.ToLower() == "epv")
                {
                    connection.InsetDataePVdata(ExeCaption_, "Jobs_USP_Stock_Price", "", "");
                }


                else if (ExeCaption_.ToLower() == "xml")
                {
                    List<url> symbols = connection.getSymbol1("xmlsymbol", "Jobs_USP_Stock_Price");
                    var XMLData = XmlConversion.SerializeToXElement(symbols);

                }
                #endregion

            }
            catch (Exception ex)
            {
                connection.InsertLogs(ExeCaption_, "StockathonCron", "StockathonCron", 0, ex.Message.ToString());
            }
            this.Close();
        }


        public List<candleList> Candle(Candle quote, Symbol symbol)
        {
            List<candleList> candleListClass = new List<candleList>();
            foreach (var cancldeC in quote.c)
            {
                candleList pp = new candleList();
                pp.stock_symbol_id = symbol.stock_symbol_id;
                pp.symbol = symbol.symbol;
                pp.c = cancldeC;
                candleListClass.Add(pp);
            }

            int i = 0;
            foreach (var cancldeH in quote.h)
            {
                candleListClass[i].h = cancldeH;
                i++;
            }
            i = 0;
            foreach (var cancldeI in quote.l)
            {
                candleListClass[i].l = cancldeI;
                i++;
            }
            i = 0;
            foreach (var cancldeo in quote.o)
            {
                candleListClass[i].o = cancldeo;
                i++;
            }
            i = 0;
            foreach (var cancldev in quote.v)
            {
                candleListClass[i].v = cancldev;
                i++;
            }
            i = 0;
            foreach (var cancldet in quote.t)
            {
                candleListClass[i].t = cancldet;
                i++;
            }
            return candleListClass;
        }

        public class XmlConversion
        {
            public static XElement SerializeToXElement(object o)
            {
                var doc = new XDocument();
                using (XmlWriter writer = doc.CreateWriter())
                {
                    XmlSerializer serializer = new XmlSerializer(o.GetType());
                    serializer.Serialize(writer, o);
                }
                return doc.Root;
            }
        }

        public static string RemoveDiacritics(string value)
        {
            var bytes = Encoding.Default.GetBytes(value);
            var text = Encoding.UTF8.GetString(bytes);
            return text;
        }


    }
}
