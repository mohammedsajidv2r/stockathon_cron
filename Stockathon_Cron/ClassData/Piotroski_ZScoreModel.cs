﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stockathon_Cron.ClassData
{
    public class Piotroski_ZScoreModel
    {
        public string symbol { get; set; }
        public string Year_Val { get; set; }
        public int? Piotroski_Score { get; set; }
        public decimal? Z_Score_mfg { get; set; }
        public decimal? Z_Score_non_mfg { get; set; }
    }
}
