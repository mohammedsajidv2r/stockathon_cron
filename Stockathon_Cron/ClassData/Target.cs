﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stockathon_Cron.ClassData
{
    public class Target
    {
        public int stock_symbol_id { get; set; }
        public string lastUpdated { get; set; }
        public string symbol { get; set; }
        public string targetHigh { get; set; }
        public string targetLow { get; set; }
        public string targetMean { get; set; }
        public string targetMedian { get; set; }
    }
}
