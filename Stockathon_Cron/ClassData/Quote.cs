﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stockathon_Cron.ClassData
{
    public class Quote
    {
        public string symbol { get; set; }
        public int stock_symbol_id { get; set; }
        public string c { get; set; }

        public string h { get; set; }

        public string l { get; set; }

        public string o { get; set; }
        public string pc { get; set; }

        public int t { get; set; }

        public DateTime unixtodate { get; set; }
    }
}
