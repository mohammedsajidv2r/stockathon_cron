﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stockathon_Cron.ClassData
{
    public class FinancialsData
    {
        public List<FinancialslistBs> FinancialsBs { get; set; }
        public List<FinancialslistIc> FinancialslistIc { get; set; }
        public List<FinancialslistCf> FinancialslistCf { get; set; }
    }

    public class FinancialslistBs
    {
        public string freq { get; set; }
        public string symbol { get; set; }
        public long? stock_symbol_id { get; set; }

        public string year { get; set; }
        public string period { get; set; }
        public string period_year { get; set; }
        public string period_month { get; set; }
        public string period_day { get; set; }
        public string accountsPayable { get; set; }
        public string accountsReceivables { get; set; }
        public string accruedLiability { get; set; }
        public string accumulatedDepreciation { get; set; }
        public string cash { get; set; }
        public string cashEquivalents { get; set; }
        public string cashShortTermInvestments { get; set; }
        public string commonStock { get; set; }
        public string currentAssets { get; set; }
        public string currentLiabilities { get; set; }
        public string currentPortionLongTermDebt { get; set; }
        public string deferredIncomeTax { get; set; }
        public string goodwill { get; set; }
        public string intangiblesAssets { get; set; }
        public string inventory { get; set; }
        public string liabilitiesShareholdersEquity { get; set; }
        public string longTermDebt { get; set; }
        public string longTermInvestments { get; set; }
        public string nonRedeemablePreferredStock { get; set; }
        public string otherCurrentAssets { get; set; }
        public string otherCurrentliabilities { get; set; }
        public string otherEquity { get; set; }
        public string otherLiabilities { get; set; }
        public string otherLongTermAssets { get; set; }
        public string otherReceivables { get; set; }

        public string preferredSharesOutstanding { get; set; }
        public string propertyPlantEquipment { get; set; }
        public string retainedEarnings { get; set; }
        public string sharesOutstanding { get; set; }
        public string shortTermDebt { get; set; }
        public string shortTermInvestments { get; set; }
        public string tangibleBookValueperShare { get; set; }
        public string totalAssets { get; set; }
        public string totalDebt { get; set; }
        public string totalEquity { get; set; }
        public string totalLiabilities { get; set; }
        public string totalLongTermDebt { get; set; }
        public string totalReceivables { get; set; }
        public string unrealizedProfitLossSecurity { get; set; }
        public string treasuryStock { get; set; }
        public string noteReceivableLongTerm { get; set; }

    }


    public class FinancialslistIc
    {
        public string freq { get; set; }
        public string symbol { get; set; }
        public long? stock_symbol_id { get; set; }

        public string year { get; set; }
        public string period_year { get; set; }
        public string period_month { get; set; }
        public string period_day { get; set; }
        public string costOfGoodsSold { get; set; }
        public string dilutedAverageSharesOutstanding { get; set; } 
        public string dilutedEPS { get; set; }
        public string ebit { get; set; } 
        public string grossIncome { get; set; }     
        public string interestIncomeExpense { get; set; }       
        public string netIncome { get; set; }               
        public string netIncomeAfterTaxes { get; set; }        
        public string period { get; set; }                    
        public string pretaxIncome { get; set; }            
        public string provisionforIncomeTaxes { get; set; }    
        public string researchDevelopment { get; set; }       
        public string revenue { get; set; }                    
        public string sgaExpense { get; set; }             
        public string totalOperatingExpense { get; set; }      
        public string totalOtherIncomeExpenseNet { get; set; } 
        public string gainLossSaleOfAssets { get; set; } 
                                                              
    }

    public class FinancialslistCf
    {
        public string freq { get; set; }
        public string symbol { get; set; }
        public long? stock_symbol_id { get; set; }

        public string year { get; set; }

        public string period_year { get; set; }
        public string period_month { get; set; }
        public string period_day { get; set; }

        public string capex                              {get;set;}  
        public string cashDividendsPaid                  {get;set;}  
        public string cashInterestPaid                   {get;set;}  
        public string cashTaxesPaid                      {get;set;}  
        public string changeinCash                       {get;set;}  
        public string changesinWorkingCapital            {get;set;}  
        public string deferredTaxesInvestmentTaxCredit   {get;set;}  
        public string depreciationAmortization           {get;set;}  
        public string foreignExchangeEffects             {get;set;}  
        public string issuanceReductionCapitalStock      {get;set;}  
        public string issuanceReductionDebtNet           {get;set;}  
        public string netCashFinancingActivities         {get;set;}  
        public string netIncomeStartingLine              {get;set;}  
        public string netInvestingCashFlow               {get;set;}  
        public string netOperatingCashFlow               {get;set;}  
        public string otherFundsFinancingItems           {get;set;}  
        public string otherFundsNonCashItems             {get;set;}  
        public string otherInvestingCashFlowItemsTotal   {get;set;}  
        public string period { get; set; }  
        public string amortization { get; set; }  
    }
}
