﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stockathon_Cron.ClassData
{

    public class Candle
    {
        public List<string> c { get; set; }
        public List<string> h { get; set; }
        public List<string> l { get; set; }
        public List<string> o { get; set; }
        public List<string> t { get; set; }
        public List<string> v { get; set; }

        public string symbol { get; set; }
        public int stock_symbol_id { get; set; }
    }

    public class candleList
    {
        public int stock_symbol_id { get; set; }
        public string symbol { get; set; }
        public string c { get; set; }
        public string h { get; set; }

        public string l { get; set; }
        public string o { get; set; }
        public string t { get; set; }
        public string v { get; set; }
    }
}
