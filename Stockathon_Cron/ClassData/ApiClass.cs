﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using US_Stock_Cron.ClassData;

namespace Stockathon_Cron.ClassData
{
    public class ApiClass
    {
        public static List<Symbol> StockSymbols(FinnSettings settings, string exchange)
        {
            List<Symbol> j = new List<Symbol>();
            try
            {
                string requestURL = settings.BaseURL + settings.Version + "/stock/symbol?exchange=" + exchange;
                WebClient client = new WebClient();
                client.Headers.Set("X-Finnhub-Token", settings.ApiKey);
                var json = client.DownloadString(requestURL);
                j = JsonConvert.DeserializeObject<List<Symbol>>(json).Where(r => r.type == "EQS").ToList();

            }
            catch (Exception ex)
            {
            }
            return j;
        }

        public static CompanyProfile CompanyProfile(FinnSettings settings, string ticker)
        {
            CompanyProfile j = new CompanyProfile();
            try
            {
                string requestURL = settings.BaseURL + settings.Version + "/stock/profile?symbol=" + ticker;
                WebClient client = new WebClient();
                client.Headers.Set("X-Finnhub-Token", settings.ApiKey);
                var json = client.DownloadString(requestURL);
                j = JsonConvert.DeserializeObject<CompanyProfile>(json);

            }
            catch (Exception ex)
            {
            }
            return j;
        }
        public static Tbl_executive Executive(FinnSettings settings, string ticker)
        {
            Tbl_executive j = new Tbl_executive();
            try
            {
                string requestURL = settings.BaseURL + settings.Version + "/stock/executive?symbol=" + ticker;
                WebClient client = new WebClient();
                client.Headers.Set("X-Finnhub-Token", settings.ApiKey);
                var json = client.DownloadString(requestURL);
                j = JsonConvert.DeserializeObject<Tbl_executive>(json);
            }
            catch (Exception ex)
            {
            }
            return j;
        }

        public static List<String> Peers(FinnSettings settings, string ticker)
        {
            List<String> j = new List<string>();
            try
            {
                string requestURL = settings.BaseURL + settings.Version + "/stock/peers?symbol=" + ticker;
                WebClient client = new WebClient();
                client.Headers.Set("X-Finnhub-Token", settings.ApiKey);
                var json = client.DownloadString(requestURL);
                j = JsonConvert.DeserializeObject<List<String>>(json);

            }
            catch (Exception ex)
            {
            }
            return j;
        }

        public static OwnershipDetails Ownership(FinnSettings settings, string ticker, string limit, string type)
        {
            OwnershipDetails j = new OwnershipDetails();
            try
            {
                //string requestURL = settings.BaseURL + settings.Version + "/stock/ownership?symbol=" + ticker+ "&limit="+ limit;
                string requestURL = "";
                if (type == "fund")
                {
                    requestURL = settings.BaseURL + settings.Version + "/stock/fund-ownership?symbol=" + ticker;
                }
                else
                {
                    requestURL = settings.BaseURL + settings.Version + "/stock/ownership?symbol=" + ticker+ "&limit=" + limit;
                }
                WebClient client = new WebClient();
                client.Headers.Set("X-Finnhub-Token", settings.ApiKey);
                var json = client.DownloadString(requestURL);
                j = JsonConvert.DeserializeObject<OwnershipDetails>(json);
            }
            catch (Exception ex)
            {

            }
            return j;
        }

        public static FinancialsData Financials(FinnSettings settings, string ticker, string statement, string freq)
        {
            FinancialsData j = new FinancialsData();
            try
            {
                string requestURL = "";
                requestURL = settings.BaseURL + settings.Version + "/stock/financials?symbol=" + ticker + "&statement=" + statement + "&freq=" + freq;
                WebClient client = new WebClient();
                client.Headers.Set("X-Finnhub-Token", settings.ApiKey);
                var json = client.DownloadString(requestURL);
                if (statement == "bs")
                {
                    json = json.Replace("financials", "FinancialsBs");
                }
                if (statement == "ic")
                {
                    json = json.Replace("financials", "FinancialslistIc");
                }
                if (statement == "cf")
                {
                    json = json.Replace("financials", "FinancialslistCf");
                }
                j = JsonConvert.DeserializeObject<FinancialsData>(json);
            }
            catch (Exception ex)
            {
            }
            return j;
        }

        public static Quote Quote(FinnSettings settings, string ticker)
        {
            Quote j = new Quote();
            try
            {
                string requestURL = settings.BaseURL + settings.Version + "/quote?symbol=" + ticker;
                WebClient client = new WebClient();
                client.Headers.Set("X-Finnhub-Token", settings.ApiKey);
                var json = client.DownloadString(requestURL);
                j = JsonConvert.DeserializeObject<Quote>(json);

            }
            catch (Exception ex)
            {

            }
            return j;
        }

        public static Target Target(FinnSettings settings, string ticker)
        {
            Target j = new Target();
            try
            {
                string requestURL = settings.BaseURL + settings.Version + "/stock/price-target?symbol=" + ticker;
                WebClient client = new WebClient();
                client.Headers.Set("X-Finnhub-Token", settings.ApiKey);
                var json = client.DownloadString(requestURL);
                j = JsonConvert.DeserializeObject<Target>(json);
            }
            catch (Exception ex)
            {

            }
            return j;
        }


        public static Candle Candle(FinnSettings settings, string ticker, string resolution, string from, string to)
        {
            Candle j = new Candle();
            try
            {
                string requestURL = settings.BaseURL + settings.Version + "/stock/candle?symbol=" + ticker + "&resolution=" + resolution + "&from=" + from + "&to=" + to;
                WebClient client = new WebClient();
                client.Headers.Set("X-Finnhub-Token", settings.ApiKey);
                var json = client.DownloadString(requestURL);
                j = JsonConvert.DeserializeObject<Candle>(json);
            }
            catch (Exception ex)
            {

            }
            return j;
        }

        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            DateTime unixStart = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            long unixTimeStampInTicks = (long)(unixTimeStamp * TimeSpan.TicksPerSecond);
            return new DateTime(unixStart.Ticks + unixTimeStampInTicks, System.DateTimeKind.Utc);
        }

        public static List<News> News(FinnSettings settings, string category)
        {
            List<News> j = new List<News>();
            try
            {
                string requestURL = settings.BaseURL + settings.Version + "/news?category=" + category;
                WebClient client = new WebClient();
                client.Headers.Set("X-Finnhub-Token", settings.ApiKey);
                var json = client.DownloadString(requestURL);
                j = JsonConvert.DeserializeObject<List<News>>(json);
            }
            catch (Exception ex)
            {

            }
            return j;
        }


        public static List<Dividend> Dividend(FinnSettings settings, string ticker, string from, string to)
        {
            List<Dividend> j = new List<Dividend>();
            try
            {
                string requestURL = settings.BaseURL + settings.Version + "/stock/dividend?symbol=" + ticker + "&from=" + from + "&to=" + to;
                WebClient client = new WebClient();
                client.Headers.Set("X-Finnhub-Token", settings.ApiKey);
                var json = client.DownloadString(requestURL);
                j = JsonConvert.DeserializeObject<List<Dividend>>(json);
            }
            catch (Exception ex)
            {

            }
            return j;
        }
        public static BasicFinancials BasicFinancials(FinnSettings settings, string ticker, string metric)
        {
            BasicFinancials j = new BasicFinancials();
            try
            {
                var jsonSettings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };
                string requestURL = settings.BaseURL + settings.Version + "/stock/metric?symbol=" + ticker + "&metric=" + metric;
                WebClient client = new WebClient();
                client.Headers.Set("X-Finnhub-Token", settings.ApiKey);
                var json = client.DownloadString(requestURL);
                j = JsonConvert.DeserializeObject<BasicFinancials>(json, jsonSettings);
            }
            catch (Exception ex)
            {
            }
            return j;
        }

        public static List<Filing> Filings(FinnSettings settings, string ticker)
        {
            string requestURL = settings.BaseURL + settings.Version + "/stock/filings?symbol=" + ticker;
            WebClient client = new WebClient();
            client.Headers.Set("X-Finnhub-Token", settings.ApiKey);
            var json = client.DownloadString(requestURL);
            List<Filing> j = JsonConvert.DeserializeObject<List<Filing>>(json).Where(x=>x.form== "10-K").ToList();
            return j;
        }
    }
}
