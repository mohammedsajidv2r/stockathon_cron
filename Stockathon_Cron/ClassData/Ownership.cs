﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stockathon_Cron.ClassData
{
    public class OwnershipDetails
    {
        public List<Ownership> Ownership { get; set; }
       
    }

    public class Ownership
    {
        public long ownership_id { get; set; }
        public long fund_ownership_id { get; set; }

        public string name { get; set; }

        public long? share { get; set; }

        public long? change { get; set; }

        public DateTime? filingDate { get; set; }

        public string symbol { get; set; }

        public long? stock_symbol_id { get; set; }

        public DateTime? created_at { get; set; }
    }
}
