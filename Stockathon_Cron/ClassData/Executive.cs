﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stockathon_Cron.ClassData
{
    public class Tbl_executive
    {
        public List<executive> executive { get; set; }
    }

    public class executive
    {
        public int? age { get; set; }

        public long? compensation { get; set; }

        public string currency { get; set; }

        public string name { get; set; }

        public string position { get; set; }

        public string sex { get; set; }

        public string since { get; set; }

        public string symbol { get; set; }

        public long? stock_symbol_id { get; set; }

        public DateTime? created_at { get; set; }

        public long executive_id { get; set; }
    }

    //public class peers
    //{
    //    public List<peerList> peerList { get; set; }
    //}
    //
    public class peerList
    {
        public string peer { get; set; }
        public string symbol { get; set; }
        public long? stock_symbol_id { get; set; }
    }
}
