﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using US_Stock_Cron.ClassData;

namespace Stockathon_Cron.ClassData
{
    public class BaseConnection
    {
        static string connectionString = ConfigurationManager.AppSettings["connectionString"];

        public void InsetData(string Type, string sp, string xml, string param)
        {
            string RES = "";
            IDbConnection con = new SqlConnection(connectionString);
            try
            {
                DynamicParameters parameters1 = new DynamicParameters();
                parameters1.Add("@RequestType", Type);
                parameters1.Add("@XmlInput", xml);
                parameters1.Add("@param", param);
                RES = SqlMapper.Query<string>(con, sp, param: parameters1, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            catch (Exception ex)
            {
                InsertLogs(Type, "InsetData", sp, 0, ex.Message.ToString());
            }
        }

        public List<Symbol> getSymbol(string Type, string sp)
        {
            IDbConnection con = new SqlConnection(connectionString);
            List<Symbol> matchid = new List<Symbol>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", Type);
                matchid = SqlMapper.Query<Symbol>(con, sp, param: parameters, commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                InsertLogs(Type, "getSymbol", sp, 0, ex.Message.ToString());
            }
            return matchid;
        }

        public List<url> getSymbol1(string Type, string sp)
        {
            IDbConnection con = new SqlConnection(connectionString);
            List<url> matchid = new List<url>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", Type);
                matchid = SqlMapper.Query<url>(con, sp, param: parameters, commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                InsertLogs(Type, "getSymbol", sp, 0, ex.Message.ToString());
            }
            return matchid;
        }

        public bool InsertLogs(string RequestType, string Name, string Type, int? ErrorLine, string Error_message)
        {
            string RES = "";
            IDbConnection con = new SqlConnection(connectionString);
            try
            {
                DynamicParameters parameters1 = new DynamicParameters();
                parameters1.Add("@Request_type", RequestType);
                parameters1.Add("@Name", Name);
                parameters1.Add("@Type", Type);
                parameters1.Add("@Error_Line", ErrorLine);
                parameters1.Add("@ErrorMsg", Error_message);
                RES = SqlMapper.Query<string>(con, "USP_Logs", param: parameters1, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            catch (Exception ex)
            {
            }
            return true;
        }

        public bool InsetDatapiotroskidata(string Type, string sp, string xml, string param)
        {
            IDbConnection con = new SqlConnection(connectionString);

            List<Symbol> symbols = getSymbol("FinSymbol", "Jobs_USP_Stock_Calculation");

            DynamicParameters para = new DynamicParameters();
            para.Add("@RequestType", "Tbl_Financial_Bs");
            List<FinancialslistBs> lstBalance = SqlMapper.Query<FinancialslistBs>(con, "Jobs_USP_Stock_Calculation", param: para, commandType: CommandType.StoredProcedure).ToList();

            DynamicParameters para1 = new DynamicParameters();
            para1.Add("@RequestType", "Tbl_Financial_Ic");
            List<FinancialslistIc> lstProfit = SqlMapper.Query<FinancialslistIc>(con, "Jobs_USP_Stock_Calculation", param: para1, commandType: CommandType.StoredProcedure).ToList();


            DynamicParameters par21 = new DynamicParameters();
            par21.Add("@RequestType", "Tbl_Financial_Cf");
            List<FinancialslistCf> lstcashflow = SqlMapper.Query<FinancialslistCf>(con, "Jobs_USP_Stock_Calculation", param: par21, commandType: CommandType.StoredProcedure).ToList();

            List<Piotroski_ZScoreModel> lstPiotroski_ZScoreModel = new List<Piotroski_ZScoreModel>();
            string RES = "";
            int i = 1;
            string symbol = "";
            try
            {
                foreach (var item_symbol in symbols.OrderBy(x => x.symbol))
                {
                    string Yearcurr = lstProfit.Where(r => r.symbol == item_symbol.symbol).OrderByDescending(r => r.period).Select(r => r.period).FirstOrDefault();
                    string YearPrev = lstProfit.OrderByDescending(r => r.period).Where(r => r.symbol == item_symbol.symbol && r.period != Yearcurr).Select(r => r.period).FirstOrDefault();

                    symbol = item_symbol.symbol;
                    FinancialslistIc item = lstProfit.Where(r => r.period == Yearcurr && r.symbol == item_symbol.symbol).FirstOrDefault();
                    FinancialslistIc itemPrev = lstProfit.Where(r => r.period == YearPrev && r.symbol == item_symbol.symbol).FirstOrDefault();
                    Piotroski_ZScoreModel Model = new Piotroski_ZScoreModel();

                    FinancialslistBs BalanceData = lstBalance.Where(r => r.period == Yearcurr && r.symbol == item_symbol.symbol).FirstOrDefault();
                    FinancialslistBs BalanceDataPrev = lstBalance.Where(r => r.period == YearPrev && r.symbol == item_symbol.symbol).FirstOrDefault();

                    //FinancialslistCf netcashflow = lstcashflow.Where(r => r.period == Yearcurr && r.symbol == item_symbol.symbol).FirstOrDefault();
                    FinancialslistCf netcashflow = lstcashflow.Where(r => r.symbol == item_symbol.symbol).OrderByDescending(r => r.period).FirstOrDefault();

                    int PiotroskiScore = 0;
                    //1 point if RoA is positive. RoA =  REPORTED PAT / Total Assets. where Total Assets = Share Capital +  Reserves & Surplus  REPORTED PAT = netIncome
                    if (decimal.Parse(BalanceData.totalAssets) != 0)
                    {
                        if (decimal.Parse(item.netIncome) / decimal.Parse(BalanceData.totalAssets) >= 0)
                        {
                            PiotroskiScore = PiotroskiScore + 1;
                        }
                    }


                    //2 point if RoA of Current Year (CY) > RoA of last year.
                    if (decimal.Parse(BalanceData.totalAssets) != 0 && decimal.Parse(BalanceDataPrev.totalAssets) != 0)
                    {
                        if ((decimal.Parse(item.netIncome) / decimal.Parse(BalanceData.totalAssets)) > (decimal.Parse(itemPrev.netIncome) / decimal.Parse(BalanceDataPrev.totalAssets)))
                        {
                            PiotroskiScore = PiotroskiScore + 1;
                        }
                    }

                    if (netcashflow != null)
                    {
                        //3 point if Net Cash Flow from Operations is positive
                        if (Convert.ToDecimal(netcashflow.netOperatingCashFlow) >= 0)
                        {
                            PiotroskiScore = PiotroskiScore + 1;
                        }

                        //4 point if Net Cash Flow from Operations > REPORTED PAT
                        if (Convert.ToDecimal(netcashflow.netOperatingCashFlow) > decimal.Parse(item.netIncome))
                        {
                            PiotroskiScore = PiotroskiScore + 1;
                        }
                    }

                    //doubt
                    //5 point if Debt/Equity of CY < Debt/Equity of last year or D/E ratio of CY < 0.01; where Debt/Equity = (Secured Loan + Unsecured Loan) / Total Assets
                    //decimal.Parse()

                    if ((decimal.Parse(BalanceData.totalEquity)) != 0 && (decimal.Parse(BalanceDataPrev.totalEquity)) != 0)
                    {
                        if (((decimal.Parse(BalanceData.totalLongTermDebt) / decimal.Parse(BalanceData.totalEquity)) <
                             (decimal.Parse(BalanceDataPrev.totalLongTermDebt) / decimal.Parse(BalanceDataPrev.totalEquity)))
                             || ((decimal.Parse(BalanceData.totalLongTermDebt) / decimal.Parse(BalanceData.totalEquity)) < (decimal)0.01))
                        {
                            PiotroskiScore = PiotroskiScore + 1;
                        }
                    }


                    //6 point if CURRENT RATIO(CY) > CURRENT RATIO(LY); where CURRENT RATIO = Total Current Assets / Total Current Liabilities
                    if ((decimal.Parse(BalanceData.currentLiabilities)) != 0 && (decimal.Parse(BalanceDataPrev.currentLiabilities)) != 0)
                    {
                        if ((decimal.Parse(BalanceData.currentAssets) / decimal.Parse(BalanceData.currentLiabilities)) > (decimal.Parse(BalanceDataPrev.currentAssets) / decimal.Parse(BalanceDataPrev.currentLiabilities)))
                        {
                            PiotroskiScore = PiotroskiScore + 1;
                        }
                    }

                    //7 point if NO NEW SHARE ISSUED IN (CY); Check  Shares in Issue (Lakhs) is less than or equal to last year value
                    if (decimal.Parse(item.dilutedAverageSharesOutstanding) <= decimal.Parse(itemPrev.dilutedAverageSharesOutstanding))
                    {
                        PiotroskiScore = PiotroskiScore + 1;
                    }

                    //8 point if OPM (CY) > OPM (LY). Operating Margin = Operating Profit / NET SALES
                    //Operating Profit = EBIT
                    if (decimal.Parse(item.revenue) != 0 && decimal.Parse(itemPrev.revenue) != 0)
                    {
                        if ((decimal.Parse(item.ebit) / decimal.Parse(item.revenue)) > (decimal.Parse(itemPrev.ebit) / decimal.Parse(itemPrev.revenue)))
                        {
                            PiotroskiScore = PiotroskiScore + 1;
                        }
                    }

                    //9 point if Asset Turnover Ratio (CY) > Asset Turnover Ratio (LY); where Asset Turnover Ratio = NET SALES / Total Assets
                    if (decimal.Parse(BalanceData.totalAssets) != 0 && decimal.Parse(BalanceDataPrev.totalAssets) != 0)
                    {
                        if ((decimal.Parse(item.revenue) / decimal.Parse(BalanceData.totalAssets)) > (decimal.Parse(itemPrev.revenue) / decimal.Parse(BalanceDataPrev.totalAssets)))
                        {
                            PiotroskiScore = PiotroskiScore + 1;
                        }
                    }

                    //---------------end of piotroski point--------------------------//

                    try
                    {
                        decimal? A, B, C, D, E = 0;
                        //A = Working Capital / Total Assets; where Working Capital = Total Current Assets - Total Current Liabilities
                        if (decimal.Parse(BalanceData.totalAssets) != 0)
                        {
                            A = (decimal.Parse(BalanceData.currentAssets) - decimal.Parse(BalanceData.currentLiabilities)) / decimal.Parse((BalanceData.totalAssets));
                        }
                        else
                        {
                            A = 0;
                        }

                        //B = Retained Earnings / Total Assets; where Retained Earnings =  REPORTED PAT - Preference Dividend - Equity Dividend
                        
                        if (decimal.Parse(BalanceData.totalAssets) != 0)
                        {
                            decimal RetainedEarnings = decimal.Parse(item.netIncome) + (decimal.Parse(netcashflow.cashDividendsPaid));
                            B = RetainedEarnings /  decimal.Parse(BalanceData.totalAssets);
                            //B = (decimal.Parse(item.netIncome) - (decimal.Parse(netcashflow.cashDividendsPaid))) / decimal.Parse(BalanceData.totalAssets);
                        }
                        else
                        {
                            B = 0;
                        }


                        //C = EBIT / Total Assets
                        if (decimal.Parse(BalanceData.totalAssets) != 0)
                        {
                            C = decimal.Parse(item.ebit) / decimal.Parse(BalanceData.totalAssets);
                        }
                        else
                        {
                            C = 0;
                        }


                        //D = Market Cap / Total Assets
                        if (decimal.Parse(BalanceData.totalAssets) != 0)
                        {
                            D = Convert.ToDecimal(item_symbol.description) / decimal.Parse(BalanceData.totalAssets);
                        }
                        else
                        {
                            D = 0;
                        }

                        //E = NET SALES / Total Assets
                        if (decimal.Parse(BalanceData.totalAssets) != 0)
                        {
                            //E = decimal.Parse(item.netIncome) / decimal.Parse(BalanceData.totalAssets);
                            E = decimal.Parse(item.revenue) / decimal.Parse(BalanceData.totalAssets);
                        }
                        else
                        {
                            E = 0;
                        }

                        Model.symbol = item.symbol;
                        Model.Year_Val = item.period;
                        Model.Piotroski_Score = PiotroskiScore;
                        Model.Z_Score_mfg = ((decimal)1.2 * A + (decimal)1.4 * B + (decimal)3.3 * C + (decimal)0.6 * D + (decimal)1 * E);
                        Model.Z_Score_non_mfg = ((decimal)6.56 * A + (decimal)3.26 * B + (decimal)6.72 * C + (decimal)1.05 * D);
                        lstPiotroski_ZScoreModel.Add(Model);
                    }
                    catch (Exception ex)
                    {
                        InsertLogs(Type, "InsetDatapiotroskidata", "Jobs_USP_Stock_Calculation", 0, ex.Message.ToString());
                    }
                    i++;
                }
                DynamicParameters parameters1 = new DynamicParameters();
                var xml1 = SerializeToXElement(lstPiotroski_ZScoreModel);
                parameters1.Add("@RequestType", Type);
                parameters1.Add("@XmlInput", xml1.ToString());
                RES = SqlMapper.Query<string>(con, "Jobs_USP_Stock_Calculation", param: parameters1, commandType: CommandType.StoredProcedure).FirstOrDefault();


            }
            catch (Exception ex)
            {
                string a = i.ToString() + "" + symbol;
                InsertLogs(Type, "InsetDatapiotroskidata", "Jobs_USP_Stock_Calculation" + a, 0, ex.Message.ToString());
            }
            return true;
        }

        public bool InsetDataePVdata(string Type, string sp, string xml, string param)
        {
            int i = 1;
            string symbol = "";
            try
            {
                IDbConnection con = new SqlConnection(connectionString);

                List<Symbol> symbols = getSymbol("EpvSymbol", "Jobs_USP_Stock_Calculation");

                DynamicParameters para = new DynamicParameters();
                para.Add("@RequestType", "Tbl_Financial_Bs");
                List<FinancialslistBs> lstBalance = SqlMapper.Query<FinancialslistBs>(con, "Jobs_USP_Stock_Calculation", param: para, commandType: CommandType.StoredProcedure).ToList();

                DynamicParameters para1 = new DynamicParameters();
                para1.Add("@RequestType", "Tbl_Financial_Ic");
                List<FinancialslistIc> lstProfit = SqlMapper.Query<FinancialslistIc>(con, "Jobs_USP_Stock_Calculation", param: para1, commandType: CommandType.StoredProcedure).ToList();

                List<epvmodel> lstepvmodel = new List<epvmodel>();
                string RES = "";
                foreach (var item_symbol in symbols.OrderBy(x => x.symbol))
                {

                    epvmodel Model = new epvmodel();
                    symbol = item_symbol.symbol;
                    List<FinancialslistIc> itemProfit = lstProfit.Where(r => r.symbol == item_symbol.symbol).OrderByDescending(r => r.period).Take(5).ToList();

                    if (itemProfit.Count >= 5)
                    {
                        FinancialslistBs BalanceData = lstBalance.Where(r => r.symbol == item_symbol.symbol).OrderByDescending(r => r.period).FirstOrDefault();

                        //NPM = Net Profit / Revenue
                        decimal[] npm = new decimal[] { };
                        try
                        {
                            npm = itemProfit.Where(x => decimal.Parse(x.revenue) != 0).Select(x => decimal.Parse(x.netIncome) / decimal.Parse(x.revenue)).ToArray();
                        }
                        catch (Exception ex)
                        {
                            npm[0] = 0;
                        }

                        //ANPM = Simple Average of NPM of recent 5 years
                        decimal anpm;
                        try
                        {
                            anpm = npm.Average();
                        }
                        catch (Exception ex)
                        {
                            anpm = 0;
                        }

                        //Sustainable Revenue = Simple Average of Revenue (of recent 5 Years)
                        decimal SustainableRevenue = itemProfit.Select(x => decimal.Parse(x.revenue)).Average();
                        //Normalised NPM = Sustainable Revenue x ANPM x 50%
                        decimal normalisenpm = anpm * SustainableRevenue * (decimal)0.5;

                        //Ce = (Rf + b * Rp)
                        decimal Ce = (item_symbol.rf + item_symbol.beta * item_symbol.rm);

                        decimal[] taxRate = new decimal[] { };
                        try
                        {
                            taxRate = itemProfit.Where(x => decimal.Parse(x.netIncome) != 0).Select(x => decimal.Parse(x.provisionforIncomeTaxes) / decimal.Parse(x.netIncome)).ToArray();
                        }
                        catch (Exception ex)
                        {
                            taxRate[0] = 0;
                        }
                        decimal tr = taxRate.Average();

                        //Cpt = Rf + 1.5 %
                        //Cd = (1 - Tr) * Cpt
                        decimal Cd = (1 - tr) * (item_symbol.rf + (decimal)0.015);

                        //WACC = (E * Ce + D * Cd) / (E+D)
                        decimal wacc = 0;
                        if (((decimal.Parse(BalanceData.totalEquity) + decimal.Parse(BalanceData.totalLongTermDebt))) > 0)
                        {
                            wacc = (decimal.Parse(BalanceData.totalEquity) * Ce + decimal.Parse(BalanceData.totalLongTermDebt) * Cd) / (decimal.Parse(BalanceData.totalEquity) + decimal.Parse(BalanceData.totalLongTermDebt));
                        }
                        //Gross EPV = Normalised NPM / WACC 

                        decimal GrossEPV = 0;
                        if(wacc>0)
                        {
                            GrossEPV = normalisenpm / wacc;
                        }
                       

                        //EPV = Gross EPV + Cash - Debt
                        decimal epv = GrossEPV + decimal.Parse(BalanceData.currentAssets) - decimal.Parse(BalanceData.totalLongTermDebt);

                        //Intrinsic Value Based on Earning Power Value = EPV / Shares Outstanding
                        decimal EarningPowerValue = 0;
                        try
                        {
                            EarningPowerValue = epv / decimal.Parse(itemProfit.OrderByDescending(x => x.period).Where(x => decimal.Parse(x.dilutedAverageSharesOutstanding) != 0).Select(x => x.dilutedAverageSharesOutstanding).FirstOrDefault());
                        }
                        catch(Exception ex)
                        {
                            EarningPowerValue = 0;
                        }
                        

                        Model.symbol = item_symbol.symbol;
                        Model.Year_Val = itemProfit.OrderByDescending(r => r.period).Select(x => x.period).FirstOrDefault();
                        // Model.npm = npm;
                        Model.anpm = anpm;
                        Model.sustainable_revenue = SustainableRevenue;
                        Model.normalised_npm = normalisenpm;
                        //Model.tax_rate = taxRate;
                        Model.tr = tr;
                        Model.ce = Ce;
                        Model.cd = Cd;
                        Model.wacc = wacc;
                        Model.gross_epv = GrossEPV;
                        Model.epv = epv;
                        Model.earning_power_value = EarningPowerValue;
                    }
                    else
                    {
                        Model.symbol = item_symbol.symbol;
                        Model.Year_Val = itemProfit.OrderByDescending(r => r.period).Select(x => x.period).FirstOrDefault();
                        Model.anpm = 0;
                        Model.sustainable_revenue = 0;
                        Model.normalised_npm = 0;
                        Model.tr = 0;
                        Model.ce = 0;
                        Model.cd = 0;
                        Model.wacc = 0;
                        Model.gross_epv = 0;
                        Model.epv = 0;
                        Model.earning_power_value = 0;
                    }
                    lstepvmodel.Add(Model);
                    i++;
                }
                DynamicParameters parameters1 = new DynamicParameters();
                var xml1 = SerializeToXElement(lstepvmodel);
                parameters1.Add("@RequestType", Type);
                parameters1.Add("@XmlInput", xml1.ToString());
                RES = SqlMapper.Query<string>(con, "Jobs_USP_Stock_Calculation", param: parameters1, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            catch (Exception ex)
            {
                string a = i.ToString() + "" + symbol;
                InsertLogs(Type, "InsetDatapiotroskidata", "Jobs_USP_Stock_Calculation", 0, ex.Message.ToString());
            }
            return true;
        }
        public static XElement SerializeToXElement(object o)
        {
            var doc = new XDocument();
            using (XmlWriter writer = doc.CreateWriter())
            {
                XmlSerializer serializer = new XmlSerializer(o.GetType());
                serializer.Serialize(writer, o);
            }
            return doc.Root;
        }

        //.Where(x=> x.symbol== "ARWR")
    }
}
