﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stockathon_Cron.ClassData
{
    public class Dividend
    {
        public string symbol { get; set; }
        public string date { get; set; }
        public decimal amount { get; set; }
        public decimal adjustedAmount { get; set; }
        public string payDate { get; set; }
        public string recordDate { get; set; }
        public string declarationDate { get; set; }
        public string currency { get; set; }
        public long? stock_symbol_id { get; set; }
    }
}
