﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stockathon_Cron.ClassData
{
    public class Filing
    {
        public int stock_symbol_id { get; set; }
        public string accessNumber { get; set; }
        public string symbol { get; set; }
        public string cik { get; set; }
        public string form { get; set; }
        public string filedDate { get; set; }
        public string acceptedDate { get; set; }
        public string reportUrl { get; set; }
        public string filingUrl { get; set; }
    }
}
