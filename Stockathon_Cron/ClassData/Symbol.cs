﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace US_Stock_Cron.ClassData
{
    public class Symbol
    {
        public string currency { get; set; }

        public string description { get; set; }

        public string displaySymbol { get; set; }

        public string symbol { get; set; }

        public string type { get; set; }

        public int id { get; set; }
        public int stock_symbol_id { get; set; }
        public decimal beta { get; set; }
        public decimal rf { get; set; }
        public decimal rm { get; set; }
    }

    public class url
    {
        public string loc { get; set; }
        public string priority { get; set; }
    }

}
