﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace US_Stock_Cron.ClassData
{
    public class FinnSettings
    {
        public string ApiKey { get; set; }
        public string Version { get; set; }
        public string BaseURL { get; set; }
    }
}
