﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stockathon_Cron.ClassData
{
    public class epvmodel
    {
        public string symbol { get; set; }
        public string Year_Val { get; set; }

        public decimal anpm { get; set; }
        public decimal sustainable_revenue { get; set; }

        public decimal normalised_npm { get; set; }
        public decimal tr { get; set; }
        public decimal ce { get; set; }
        public decimal cd { get; set; }
        public decimal wacc { get; set; }
        public decimal gross_epv { get; set; }
        public decimal epv { get; set; }
        public decimal earning_power_value { get; set; }
    }
}
