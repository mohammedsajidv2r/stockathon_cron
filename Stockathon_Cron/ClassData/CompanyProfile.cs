﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stockathon_Cron.ClassData
{
    public class CompanyProfile
    {
        public string address { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string currency { get; set; }
        public string cusip { get; set; }
        public string sedol { get; set; }
        public string description { get; set; }
        public string employeeTotal { get; set; }
        public string exchange { get; set; }
        public string ggroup { get; set; }
        public string gind { get; set; }
        public string gsector { get; set; }
        public string gsubind { get; set; }
        public string ipo { get; set; }
        public string isin { get; set; }
        public decimal marketCapitalization { get; set; }
        public string naics { get; set; }
        public string naicsNationalIndustry { get; set; }
        public string naicsSector { get; set; }
        public string naicsSubsector { get; set; }

        public string name { get; set; }
        public string phone { get; set; }
        public float shareOutstanding { get; set; }
        public string state { get; set; }
        public string ticker { get; set; }
        public string weburl { get; set; }
        public string logo { get; set; }
        public string finnhubIndustry { get; set; }
        public string symbol { get; set; }
        public int stock_symbol_id { get; set; }
    }
}
